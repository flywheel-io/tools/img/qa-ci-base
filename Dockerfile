# define args used in FROM first
ARG PYTHON_VERSION=3.11

# download pre-built binaries to /opt/bin
FROM debian:bookworm-slim AS opt
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /opt/bin
ENV DEBIAN_FRONTEND=noninteractive \
    PATH=$PATH:/opt/bin
RUN apt-get update -yq; \
    apt-get install -yq --no-install-recommends ca-certificates curl xz-utils
RUN curl -fLSs https://zyedidia.github.io/eget.sh | sh; eget --help &>/dev/null
RUN eget astral-sh/ruff -agnu; ruff --help &>/dev/null
RUN eget astral-sh/uv -agnu; uv --help &>/dev/null
RUN ARCH=$(uname -m | sed -E 's/arm64/aarch64/'); \
    eget chmln/sd -a"${ARCH}-unknown-linux-musl"; \
    sd --help &>/dev/null
RUN eget christian-korneck/docker-pushrm; docker-pushrm --help &>/dev/null
RUN eget controlplaneio/kubesec; kubesec --help &>/dev/null
RUN eget docker/buildx -a^json --to=docker-buildx; docker-buildx --help &>/dev/null
RUN eget docker/compose --to=docker-compose; docker-compose --help &>/dev/null
RUN eget ducaale/xh; xh --help &>/dev/null
RUN eget GoogleContainerTools/skaffold; skaffold --help &>/dev/null
RUN eget hadolint/hadolint; hadolint --help &>/dev/null
RUN eget itchyny/gojq; ln -s gojq jq; jq --help &>/dev/null
RUN eget koalaman/shellcheck; shellcheck --help &>/dev/null
RUN eget mikefarah/yq -a^tar; yq --help &>/dev/null
RUN eget mvdan/sh --to=shfmt; shfmt --help &>/dev/null
RUN eget norwoodj/helm-docs -atar; helm-docs --help &>/dev/null
RUN eget shteou/helm-dependency-fetch; helm-dependency-fetch --help &>/dev/null
RUN eget TomWright/dasel -a^gz; dasel --help &>/dev/null
RUN eget yannh/kubeconform; kubeconform -h &>/dev/null
RUN V_GITLAB_RELEASE_CLI=v0.16.0; \
    ARCH=$(uname -m | sed -E 's/x86_64/amd64/'); \
    eget "https://gitlab.com/gitlab-org/release-cli/-/releases/${V_GITLAB_RELEASE_CLI}/downloads/bin/release-cli-linux-${ARCH}" --to=release-cli; \
    release-cli --help &>/dev/null || :
RUN V_GLAB=1.40.0; \
    ARCH=$(uname -m | sed -E 's/aarch64/arm64/'); \
    eget "https://gitlab.com/gitlab-org/cli/-/releases/v${V_GLAB}/downloads/glab_${V_GLAB}_Linux_${ARCH}.tar.gz" -fglab; \
    glab --help &>/dev/null
RUN V_DOCKER=26.0.2; \
    ARCH=$(uname -m); \
    eget "https://download.docker.com/linux/static/stable/${ARCH}/docker-${V_DOCKER}.tgz" -fdocker/docker; \
    docker --help &>/dev/null
RUN V_HELM=v3.14.4; \
    ARCH=$(uname -m | sed -E 's/x86_64/amd64/;s/aarch64/arm64/'); \
    eget "https://get.helm.sh/helm-${V_HELM}-linux-${ARCH}.tar.gz" -fhelm; \
    helm --help &>/dev/null
RUN V_NODEJS=21.7.3; \
    ARCH=$(uname -m | sed -E 's/x86_64/x64/;s/aarch64/arm64/'); \
    curl -fLSs "https://nodejs.org/dist/v${V_NODEJS}/node-v${V_NODEJS}-linux-${ARCH}.tar.xz" \
        | tar xJC /opt --no-same-owner --strip-components=1 "node-v${V_NODEJS}-linux-${ARCH}"/{bin,include,lib,share}; \
    npm help &>/dev/null

# build the python-version variant
FROM python:${PYTHON_VERSION}-slim-bookworm AS img
SHELL ["/bin/bash", "-Eeuxo", "pipefail", "-c"]
CMD ["/bin/bash"]
RUN apt-get update -yq; \
    apt-get upgrade -yq; \
    apt-get install -yq --no-install-recommends \
        bsdmainutils curl git jo make mbuffer moreutils openssh-client unzip xz-utils zip zstd \
        containernetworking-plugins fuse-overlayfs golang-github-containernetworking-plugin-dnsname podman skopeo; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*
RUN mkdir -m700 ~/.ssh; \
    ssh-keyscan -H gitlab.com >>~/.ssh/known_hosts; \
    update-alternatives --set iptables /usr/sbin/iptables-legacy; \
    mkdir -p /etc/containers; \
    curl -fLSso/etc/containers/containers.conf https://raw.githubusercontent.com/containers/image_build/main/podman/containers.conf; \
    curl -fLSso/etc/containers/storage.conf https://raw.githubusercontent.com/containers/podman/main/vendor/github.com/containers/storage/storage.conf; \
    chmod 644 /etc/containers/*.conf; \
    mkdir -p /var/lib/shared/{overlay,vfs}-{images,layers}; \
    touch /var/lib/shared/{overlay,vfs}-{images/images,layers/layers}.lock; \
    sed -Ei /etc/containers/registries.conf \
        -e's|^# unqualified-search-registries = .*|unqualified-search-registries = ["docker.io"]|'; \
    sed -Ei /etc/containers/storage.conf \
        -e's|^#mount_program|mount_program|' \
        -e's|^mountopt = .*|mountopt = "nodev,fsync=0"|' \
        -e'/^additionalimagestores = .*/a "/var/lib/shared",'
COPY --from=opt /opt /opt
ENV BUILDAH_FORMAT=docker \
    BUILDX_EXPERIMENTAL=1 \
    DEBIAN_FRONTEND=noninteractive \
    DOCKER_BUILDKIT=1 \
    GIT_TERMINAL_PROMPT=0 \
    LANG=C.UTF-8 \
    LANGUAGE=en_US \
    LC_ALL=C.UTF-8 \
    NPM_CONFIG_FUND=false \
    PATH=$PATH:/opt/bin \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_ROOT_USER_ACTION=ignore \
    POETRY_INSTALLER_MAX_WORKERS=10 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VIRTUALENVS_IN_PROJECT=false \
    POETRY_WARNINGS_EXPORT=false
RUN mkdir -p ~/.docker/cli-plugins; ln -s /opt/bin/docker-* ~/.docker/cli-plugins; \
    helm plugin install https://github.com/chartmuseum/helm-push; \
    uv pip install --no-cache --system --upgrade \
        jsonschema pip podman-compose poetry poetry-plugin-export pre-commit setuptools wheel yamllint; \
    npm install --location=global --omit=dev \
        @prantlf/jsonlint markdownlint-cli2 npm prettier pyright; \
    rm -rf ~/.{cache,npm} /tmp/*
