# use a moving qa-ci ref to avoid circular dep update
include:
  - project: flywheel-io/tools/etc/qa-ci
    file: ci/img.yml
    ref: main

variables:
  # trigger an update in qa-ci on successful main pipelines
  UPDATE_PROJECTS: flywheel-io/tools/etc/qa-ci

# only build/publish variants on special refs and when setting the mr label
.rules:docker:
  - if: >
      $DOCKER_VARIANT &&
      $CI_COMMIT_BRANCH !~ /main|sse/ &&
      $CI_MERGE_REQUEST_LABELS !~ /build::variant/
    when: never
  - if: >
      $CI_COMMIT_BRANCH == "main" ||
      $CI_MERGE_REQUEST_LABELS =~ /build::always/
  - changes: [Dockerfile]

build:docker:
  extends: .build:docker
  rules:
    - !reference [.rules:not-on-automation-trigger]
    - !reference [.rules:docker]
  variables:
    # target amd64 for linux/ci/old macs *and* arm64 for new macs
    # TODO figure out cross-platform build errors...
    # DOCKER_BUILD_PLATFORM: linux/amd64,linux/arm64
    # pass github token secret to avoid hitting the rate limit w/ eget
    DOCKER_BUILD_EXTRA_ARGS: --secret=id=GITHUB_TOKEN,env=GITHUB_TOKEN
  before_script:
    - podman secret create --env=true GITHUB_TOKEN GITHUB_TOKEN

publish:docker:
  extends: .publish:docker
  rules:
    - !reference [.rules:not-on-automation]
    - !reference [.rules:docker]

# 3.12
# type statement, improved f-strings
# per-interpreter GIL
# distutils package removed
# setuptools not in venvs by default
.variant:3.12:
  variables:
    DOCKER_VARIANT: "3.12"
    DOCKER_BUILD_ARGS: PYTHON_VERSION=$DOCKER_VARIANT
build:docker:3.12:
  needs: [build:docker]
  extends: [build:docker, .variant:3.12]
publish:docker:3.12:
  extends: [publish:docker, .variant:3.12]

# 3.11
# exception groups and notes, tomllib, Self type
.variant:3.11:
  variables:
    DOCKER_VARIANT: "3.11"
    DOCKER_BUILD_ARGS: PYTHON_VERSION=$DOCKER_VARIANT
build:docker:3.11:
  needs: [build:docker]
  extends: [build:docker, .variant:3.11]
publish:docker:3.11:
  extends: [publish:docker, .variant:3.11]

# 3.10 (EOL: 2026-10-31)
# pattern matching: match / case
# with stmt allows parenthesis
# union typing: x | y
.variant:3.10:
  variables:
    DOCKER_VARIANT: "3.10"
    DOCKER_BUILD_ARGS: PYTHON_VERSION=$DOCKER_VARIANT
build:docker:3.10:
  needs: [build:docker]
  extends: [build:docker, .variant:3.10]
publish:docker:3.10:
  extends: [publish:docker, .variant:3.10]

# 3.9 (EOL: 2025-10-31)
# dict union operator: d1 | d2
# collection typing: list[x] dict[y, z]
# str.removeprefix / str.removesuffix
# zoneinfo
.variant:3.9:
  variables:
    DOCKER_VARIANT: "3.9"
    DOCKER_BUILD_ARGS: PYTHON_VERSION=$DOCKER_VARIANT
build:docker:3.9:
  needs: [build:docker]
  extends: [build:docker, .variant:3.9]
publish:docker:3.9:
  extends: [publish:docker, .variant:3.9]

# 3.8 (EOL: 2024-10-31)
# walrus assignment :=
# positional-only args f(a, /, ...)
# f-string debug f"{x=}"
# importlib.metadata
.variant:3.8:
  variables:
    DOCKER_VARIANT: "3.8"
    DOCKER_BUILD_ARGS: PYTHON_VERSION=$DOCKER_VARIANT
build:docker:3.8:
  needs: [build:docker]
  extends: [build:docker, .variant:3.8]
publish:docker:3.8:
  extends: [publish:docker, .variant:3.8]
