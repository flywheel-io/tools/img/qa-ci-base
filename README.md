# flywheel/qa-ci-base

Base image for [qa-ci][qa-ci], with all the heavyweight 3rd party tools installed
for building, liniting, testing and running CI jobs in the common Flywheel stack.

[qa-ci]: https://gitlab.com/flywheel-io/tools/etc/qa-ci

## Testing

```bash
pre-commit run -a
```

## Docker

Images are published on every successful CI build to [dockerhub][dockerhub].

Tagging scheme:

- python 3.12: `3.12-main.d34db33f`, `3.12-main`, `3.12`
- python 3.11: `3.11-main.d34db33f`, `3.11-main`, `3.11`, `main.d34db33f`, `main`, `latest`
- python 3.10: `3.10-main.d34db33f`, `3.10-main`, `3.10`
- python 3.9: `3.9-main.d34db33f`, `3.9-main`, `3.9`
- python 3.8: `3.8-main.d34db33f`, `3.8-main`, `3.8`

Branch-based builds are also available, where the branch name replaces `main`.

[dockerhub]: https://hub.docker.com/repository/docker/flywheel/qa-ci-base/tags?page=1&ordering=last_updated

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
